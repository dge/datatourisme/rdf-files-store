/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.jsonld;

import com.github.jsonldjava.utils.JsonUtils;
import fr.datatourisme.AbstractTest;
import org.apache.jena.query.Dataset;
import org.apache.jena.query.DatasetFactory;
import org.apache.jena.rdf.model.Literal;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.riot.RDFParser;
import org.apache.jena.riot.RIOT;
import org.apache.jena.sparql.util.Context;
import org.apache.jena.vocabulary.RDFS;
import org.junit.Assert;
import org.junit.Test;
import org.semanticweb.yars.nx.Resource;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;

public class JsonLdSerializerTest extends AbstractTest {
    @Test
    public void testSerialize() throws IOException {
        // serialize to json
        JsonLdSerializer jsonSerializer = new JsonLdSerializer(store, context);
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        jsonSerializer.serialize(new Resource("https://data.datatourisme.gouv.fr/13/c8f96a13-4c1a-33eb-a734-d0e8223dbaa8"), bos);
        bos.close();

        // set context
        Context ctx = new Context();
        Object jsonLdContextAsObject = JsonUtils.fromString(context.toJson());
        ctx.set(RIOT.JSONLD_CONTEXT, jsonLdContextAsObject);

        // parse to model
        Model model = ModelFactory.createDefaultModel();
        InputStream in = new ByteArrayInputStream(bos.toByteArray());
        RDFParser.source(in).lang(Lang.JSONLD).context(ctx).parse(model);

        // assertions
        org.apache.jena.rdf.model.Resource res = model.getResource("https://data.datatourisme.gouv.fr/13/c8f96a13-4c1a-33eb-a734-d0e8223dbaa8");
        Assert.assertTrue(res.getProperty(RDFS.comment).getObject() instanceof Literal);
        Assert.assertEquals(21, res.listProperties().toList().size());
        Assert.assertEquals(84, model.size());
    }
}
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme;

import com.conjecto.graphstore.GraphLoader;
import com.conjecto.graphstore.GraphStore;
import com.conjecto.graphstore.GraphStoreOptions;
import com.conjecto.graphstore.exception.GraphStoreException;
import fr.datatourisme.vocabulary.DatatourismeMetadata;
import fr.datatourisme.vocabulary.DatatourismeThesaurus;
import org.apache.jena.vocabulary.OWL;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.InputStream;

public class AbstractTest {
    static String DEFAULT_NAMESPACE = "https://www.datatourisme.gouv.fr/ontology/core#";

    protected GraphStore store;
    protected Context context;

    @Rule
    public TemporaryFolder temp = new TemporaryFolder();

    @Before
    public void setup() throws GraphStoreException {
        // prepare graphstore
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream("sample.nt");
        GraphStoreOptions options = (new GraphStoreOptions())
            .setDisablePOSIndex(true)
            .setCreateIfMissing(true);
        store = GraphStore.open(temp.getRoot().getAbsolutePath(), options);
        GraphLoader loader = new GraphLoader(store, "nt");
        loader.load(inputStream);
        store.compact();

        // prepare context
        // build context
        ContextBuilder ctxBuilder = new ContextBuilder(DEFAULT_NAMESPACE);
        ctxBuilder.addOntology(getClass().getClassLoader().getResource("datatourisme.ttl").getPath());
        ctxBuilder.addOntology(getClass().getClassLoader().getResource("datatourisme-extras.ttl").getPath());
        context = ctxBuilder
            .excludeNamespace(OWL.NS)
            .excludeNamespace(DatatourismeMetadata.NS)
            .addNamespace(DatatourismeThesaurus.PREFIX, DatatourismeThesaurus.NS)
            .build();
    }

}
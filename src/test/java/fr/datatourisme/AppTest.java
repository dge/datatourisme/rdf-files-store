/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme;

import com.conjecto.graphstore.exception.GraphStoreException;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.IOException;

/**
 * Unit test for simple App.
 */
public class AppTest
{
    @Test
    @Ignore
    public void testApp() throws IOException {
        String onto1 = getClass().getClassLoader().getResource("datatourisme.ttl").getPath();
        String onto2 = getClass().getClassLoader().getResource("datatourisme-extras.ttl").getPath();
        String[] args = {"-o " + onto1, "-o " + onto2, "/home/blaise/Workspace/datatourisme/java-worker/data/graphstore", "/home/blaise/Workspace/datatourisme/java-worker/data/filestore"};
        App.main(args);
    }
}

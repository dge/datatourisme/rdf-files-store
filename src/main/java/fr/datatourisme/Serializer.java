/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme;

import com.conjecto.graphstore.GraphStore;
import com.conjecto.graphstore.TripletIterator;
import org.semanticweb.yars.nx.Literal;
import org.semanticweb.yars.nx.Node;
import org.semanticweb.yars.nx.Resource;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Path;

public abstract class Serializer {
    protected GraphStore store;
    protected Context context;

    public Serializer(GraphStore store, Context context) {
        this.store = store;
        this.context = context;
    }
    public abstract void serialize(Node node, OutputStream out) throws IOException;
    public abstract void serializeIndexEntry(Node node, OutputStream out, Path filePath) throws IOException;

    /**
     * Serialize a resource to a file, with index entry in a *.idx file
     *
     * @param node
     * @param file
     * @throws IOException
     */
    public void serialize(Node node, File file, File workdir) throws IOException {
        // serialize resource
        FileOutputStream fos = new FileOutputStream(file);
        serialize(node, fos);
        fos.close();
        // serialize index entry
        File indexFile = new File(file.getParent(), file.getName() + ".idx");
        fos = new FileOutputStream(indexFile);
        serializeIndexEntry(node, fos, workdir.toPath().relativize(file.toPath()));
        fos.close();
    }

    /**
     * @param node
     * @param propertyUri
     */
    public String getFirstValue(Node node, String propertyUri) {
        Resource prop = new Resource(propertyUri);
        TripletIterator iterator = store.querySPO(node.toString(), prop.toString());
        while(iterator.hasNext()) {
            Node resource = iterator.next().getObject();
            if (!(resource instanceof Literal) || ((Literal) resource).getLanguageTag() == null || ((Literal) resource).getLanguageTag().equals("fr")) {
                return resource.getLabel();
            }
        }
        return null;
    }
}

/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme;

import com.conjecto.graphstore.PrefixMapping;
import com.conjecto.graphstore.Triplet;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import fr.datatourisme.vocabulary.DatatourismeData;
import org.apache.jena.ext.com.google.common.cache.CacheBuilder;
import org.apache.jena.ext.com.google.common.cache.CacheLoader;
import org.apache.jena.ext.com.google.common.cache.LoadingCache;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.ontology.OntProperty;
import org.apache.jena.rdf.model.ResourceFactory;
import org.apache.jena.vocabulary.RDF;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

public class Context {
    OntModel model;
    String defaultNamespace;
    PrefixMapping prefixMapping;
    List<String> excludedNamespaces;
    LoadingCache<String, Boolean> isExcludedCache = CacheBuilder.newBuilder().build(CacheLoader.from(this::isExcludedSupplier));
    LoadingCache<String, Boolean> hasLangStringRangeCache = CacheBuilder.newBuilder().build(CacheLoader.from(this::hasLangStringRangeSupplier));

    /**
     * Constructor
     *
     * @param model
     * @param defaultNamespace
     * @param additionalNamespaces
     * @param excludedNamespaces
     */
    public Context(OntModel model, String defaultNamespace, Map<String, String> additionalNamespaces, List<String> excludedNamespaces) {
        this.model = model;
        this.defaultNamespace = defaultNamespace;
        this.excludedNamespaces = excludedNamespaces;

        Map<String, String> mapping = model.getNsPrefixMap().entrySet()
            .stream()
            .filter(es -> !excludedNamespaces.contains(es.getValue()))
            .filter(es -> !es.getValue().equals(defaultNamespace))
            .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

        mapping.putAll(additionalNamespaces);
        this.prefixMapping = new PrefixMapping().set(mapping);
    }

    /**
     * Return default namespace
     *
     * @return
     */
    public String getDefaultNamespace() {
        return defaultNamespace;
    }

    /**
     * Return default language
     *
     * @return
     */
    public String getDefaultLanguage() {
        return "fr";
    }

    /**
     * Return the prefix mapping
     *
     * @return
     */
    public PrefixMapping getPrefixMapping() {
        return prefixMapping;
    }

    /**
     * Memoized supplier for isExcluded method
     *
     * @param uri
     * @return
     */
    private Boolean isExcludedSupplier(String uri) {
        return excludedNamespaces.stream().anyMatch(uri::startsWith);
    }

    /**
     * Return true if the IRI must be excluded from serialization
     *
     * @param uri
     * @return
     */
    public Boolean isExclude(String uri) {
        try {
            return isExcludedCache.get(uri);
        } catch (ExecutionException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Return true if the triple must be excluded from serialization
     *
     * @param triplet
     * @return
     */
    public boolean isExclude(Triplet triplet) {
        return isExclude(triplet.getPredicate().getLabel()) || isExclude(triplet.getObject().getLabel());
    }

    /**
     * Memoized supplier for isExcluded method
     *
     * @param uri
     * @return
     */
    private Boolean hasLangStringRangeSupplier(String uri) {
        OntProperty property = model.getOntProperty(uri);
        if (property != null) {
            return property.hasRange(RDF.langString);
        }
        return false;
    }

    /**
     * Return true if the triple must be excluded from serialization
     *
     * @return
     */
    public boolean hasLangStringRange(String uri) {
        try {
            return hasLangStringRangeCache.get(uri);
        } catch (ExecutionException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * @return
     */
    public String shortIRI(String uri) {
        if (uri.startsWith(defaultNamespace)) {
            return uri.substring(defaultNamespace.length());
        } else {
            return model.shortForm(uri);
        }
    }

    /**
     * Return true if the object resource must be embedded (iteratively serialized) .
     * Otherwise, it will just be referenced by uri
     *
     * @param triplet
     * @return
     */
    public boolean isEmbeddable(Triplet triplet) {
        // if it's attached to the special keyword @type
        if (RDF.type.getURI().equals(triplet.getPredicate().getLabel())) {
            return false;
        }
        // if it's not DATAtourisme data, don't expand it
        if (!triplet.getObject().getLabel().startsWith(DatatourismeData.NS)) {
            return false;
        }
        return true;
    }

    /**
     * Dump to JsonSLD
     *
     * @return
     */
    public String toJson() {
        JsonObject context = new JsonObject();

        // add @vocab
        //Ontology ont = model.listOntologies().next();
        if (defaultNamespace != null) {
            context.addProperty("@vocab", defaultNamespace);
        }

        // add prefixes
        model.getNsPrefixMap().forEach((prefix, uri) -> {
            if (!uri.equals(defaultNamespace)) {
                context.addProperty(prefix, uri);
            }
        });

        model.listAllOntProperties().forEachRemaining(property -> {
            JsonObject definition = new JsonObject();
            // @type
            org.apache.jena.rdf.model.Resource range = property.getRange();
            if ( range != null && !range.isAnon()) {
                definition.addProperty("@type", shortIRI(range.getURI()));
            }

            // language
            if (property.hasRange(RDF.langString)) {
                definition.addProperty("@container", "@language" );
            }

            if (definition.size() > 0) {
                context.add(shortIRI(property.getURI()), definition);
            }
        });

        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setPrettyPrinting();
        return gsonBuilder.create().toJson(context);
    }
}
